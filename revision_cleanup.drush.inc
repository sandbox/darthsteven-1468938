<?php

/**
 * Implements hook_drush_command().
 */
function revision_cleanup_drush_command() {
  $items = array();

  $items['revision-cleanup-batch'] = array(
    'description' => dt('Cleans up old revisions.'),
    'drupal dependencies' => array('revision_cleanup'),
  );
  
  return $items;
}

/**
 * Validation for the revision-cleanup-batch command.
 */
function drush_revision_cleanup_batch_validate() {
  $type = drush_get_option('type');

  if (empty($type)) {
    return drush_set_error('REVISION_CLEANUP_NO_TYPE', dt('You must specify a node type to clean up.'));
  }
}

/**
 * Drush command to cleanup revisions.
 */
function drush_revision_cleanup_batch() {
  $current_nid = drush_get_option('current_nid', NULL);
  $type = drush_get_option('type');

  if (is_null($current_nid)) {
    $current_nid = 0;
    $context = array();
    drush_log(dt('Cleaning up revisions...'));
    // We just keep going until the job is done!
    do {
      $result = drush_backend_invoke_args('revision-cleanup-batch', array(), array('current_nid' => $current_nid, 'type' => $type, 'context' => $context), 'GET', FALSE);
      $context = $result['context']['context'];
      if (isset($context['finished'])) {
        drush_log(dt('Progress: @completion%', array('@completion' => round(10000 * $context['finished']) / 100)),'ok');
      }
    } while (isset($context['finished']) && ($context['finished'] < 1));
  }
  else {
    // Cleanup some revisions.
    $sandbox = array();
    $context = drush_get_option('context');
    module_load_include('inc', 'revision_cleanup', 'revision_cleanup.admin');
    revision_cleanup_batch($type, $context);
    drush_set_option('context', $context);
  }

}