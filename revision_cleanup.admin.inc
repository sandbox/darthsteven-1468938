<?php
/**
 * @file
 * Admin bits of the revision cleanup module.
 */

/**
 * Form to clean up revisions.
 */
function revision_cleanup_form($form, &$form_state, $node_type) {
  
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $node_type->type,
  );
  
  return confirm_form($form,
      t('Cleanup revisions'),
      'admin/structure/types/manage/' . $node_type->type,
      t('Are you sure that you want to remove all non-current revisions from @type content? This is NOT a reversible operation and you will lose data!', array('@type' => $node_type->name)));
}

/**
 * Form submit callback.
 *
 * @see revision_cleanup_form()
 */
function revision_cleanup_form_submit($form, &$form_state) {
  // set ourselves up in a nice little batch.
  $batch = array(
    'title' => t('Removing non-current revisions'),
    'operations' => array(
      array('revision_cleanup_batch', array($form_state['values']['type'])),
    ),
    'finished' => 'revision_cleanup_batch_finished',
    'file' => drupal_get_path('module', 'revision_cleanup') . '/revision_cleanup.admin.inc',
  );
  batch_set($batch);   
}

/**
 * Batch callback to clean up revisions.
 */
function revision_cleanup_batch($type, &$context) {
  // We want to play in the sandbox.
  $sandbox = &$context['sandbox'];

  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['current_nid'] = 0;
    $sandbox['max'] = db_select('node', 'n')->condition('type', $type)->countQuery()->execute()->fetchField();
    $context['results']['count'] = 0;
  }

  $nodes = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('nid', $sandbox['current_nid'], '>')
    ->condition('type', $type)
    ->range(0, 10)
    ->orderBy('nid', 'ASC')
    ->execute();

  $entities = array();

  foreach ($nodes as $node) {
    $entities[] = $node->nid;
    $sandbox['progress']++;
    $sandbox['current_nid'] = $node->nid;
  }

  if (!empty($entities)) {
    $nodes = entity_load('node', $entities);
    if (!empty($nodes)) {
      foreach ($nodes as $node) {
        $revisions = node_revision_list($node);
        // Make sure we don't delete the current revisions.
        unset($revisions[$node->vid]);
        foreach ($revisions as $vid => $revision) {
          node_revision_delete($vid);
          $context['results']['count']++;
        }
      }
    }
    $context['finished'] = empty($sandbox['max']) ? 1 : ($sandbox['progress'] / $sandbox['max']);
    $context['finished'] = min(1, $context['finished']);
  }
}

/**
 * Finished callback for the batch.
 */
function revision_cleanup_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural($results['count'], 'Succesfully removed 1 old revision.', 'Succesfully removed @count old revisions.');
    drupal_set_message($message);
  }
  else {
    $message = t('Failed to remove all old revisions.');
    drupal_set_message($message, 'warning');
  } 
}